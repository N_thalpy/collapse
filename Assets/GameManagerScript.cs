﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GameManagerScript : MonoBehaviour 
{
    public GameObject MoleculePrefab;
    public GameObject PhotonPrefab;
    public GameObject PlayerPrefab;
    
    public Sprite[] MoleculeSprite;
    public float[] MoleculeColliderSize;

    public AudioSource CollisionSE;
    public AudioSource PhotonSE;

    private bool _Alert;
    private GameObject _AlertPlane;

    private List<GameObject> _MoleculeAlivelist;
    private Queue<GameObject> _MoleculeGarbage;
    private List<GameObject> _PhotonAlivelist;
    private Queue<GameObject> _PhotonGarbage;

	private enum GameFlow
	{
		Ready,
		Playing,
		Finished,
	}
    private GameFlow __GameFlow;
	private GameFlow _GameFlow
    {
        get
        {
            return __GameFlow;
        }
        set
        {
            switch (__GameFlow)
            {
                case GameFlow.Ready:
                    foreach (GameObject v in _ReadyUI)
                        v.SetActive(false);
                    break;

                case GameFlow.Playing:
                    foreach (GameObject v in _PlayingUI)
                        v.SetActive(false);
                    break;

                case GameFlow.Finished:
                    foreach (GameObject v in _FinishedUI)
                        v.SetActive(false);
                    break;
            }
            switch (value)
            {
                case GameFlow.Ready:
                    foreach (GameObject v in _ReadyUI)
                        v.SetActive(true);
                    break;

                case GameFlow.Playing:
                    foreach (GameObject v in _PlayingUI)
                        v.SetActive(true);
                    break;

                case GameFlow.Finished:
                    foreach (GameObject v in _FinishedUI)
                        v.SetActive(true);
                    break;
            }

            __GameFlow = value;
        }
    }
    private int __Score;
    private int _Score
    {
        get
        {
            return __Score;
        }
        set
        {
            __Score = value;
            _ScoreUI.GetComponent<Text>().text = _Score.ToString();
        }
    }

    private GameObject[] _ReadyUI;
    private GameObject[] _PlayingUI;
    private GameObject[] _FinishedUI;

    private GameObject _ScoreUI;
    private GameObject _SpeedUI;
    private GameObject _SpeedTextUI;
    private GameObject _TotalScoreUI;

    private int _ID;

	private void Start()
    {        
        _ReadyUI = GameObject.FindGameObjectsWithTag("ReadyUI");
        _PlayingUI = GameObject.FindGameObjectsWithTag("PlayingUI");
        _FinishedUI = GameObject.FindGameObjectsWithTag("FinishedUI");
        _GameFlow = GameFlow.Ready;

        _AlertPlane = GameObject.Find("AlertPlane");

        _ScoreUI = GameObject.Find("Score");
        _SpeedUI = GameObject.Find("Speed");
        _SpeedTextUI = GameObject.Find("SpeedText");
        _TotalScoreUI = GameObject.Find("TotalScore");

        _MoleculeAlivelist = new List<GameObject>();
        _MoleculeGarbage = new Queue<GameObject>();
        _PhotonAlivelist = new List<GameObject>();
        _PhotonGarbage = new Queue<GameObject>();
        
        _Score = 0;
        _ID = 0;
        
        foreach (GameObject v in _PlayingUI)
            v.SetActive(false);
        foreach (GameObject v in _FinishedUI)
            v.SetActive(false);
    }
    private void Update()
    {
        if (_GameFlow == GameFlow.Playing)
        {
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player>())
            {
                float speed = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().speed;
                _SpeedTextUI.GetComponent<Text>().text = speed.ToString();
                _SpeedUI.GetComponent<Slider>().value = speed;
            }
        }
        if (_AlertPlane != null)
        {
            Material mat = _AlertPlane.GetComponent<MeshRenderer>().materials[0];
            if (_Alert == true)
            {
                Color color = mat.color;
                mat.color = new Color(color.r, color.g, color.b, color.a + 0.05f);

                if (mat.color.a > 0.5f)
                    _Alert = false;
            }
            else if (mat.color.a > 0.05f)
            {
                Color color = mat.color;
                mat.color = new Color(color.r, color.g, color.b, color.a - 0.05f);
            }
        }
    }
    private int GetID()
    {
        _ID++;
        return _ID;
    }
    public GameObject CreateMolecule(int size, Vector3 position)
    {
        GameObject molecule = null;

        if (_MoleculeGarbage.Count != 0)
            molecule = _MoleculeGarbage.Dequeue();
        else
            molecule = Instantiate(MoleculePrefab);

        molecule.GetComponent<Molecule>().Reset(GetID(), MoleculeSprite[size], MoleculeColliderSize[size], size);
        molecule.transform.position = position;
        _MoleculeAlivelist.Add(molecule);
        return molecule;
    }
    public void DestroyMolecule(GameObject molecule)
    {
        molecule.gameObject.SetActive(false);

        _MoleculeAlivelist.Remove(molecule);
        _MoleculeGarbage.Enqueue(molecule);
    }
    public GameObject CreatePhoton(Vector3 position)
    {
        GameObject photon = null;

        if (_PhotonGarbage.Count != 0)
            photon = _PhotonGarbage.Dequeue();
        else
            photon = Instantiate(PhotonPrefab);

        photon.transform.position = position;
        photon.GetComponent<Photon>().Reset();
        photon.transform.position = position;
        _PhotonAlivelist.Add(photon);
        return photon;
    }
    public void DestroyPhoton(GameObject photon)
    {
        photon.gameObject.SetActive(false);

        _PhotonAlivelist.Remove(photon);
        _PhotonGarbage.Enqueue(photon);
    }
    public void IncreaseScore(int amount)
    {
        if (amount < 0)
            throw new ArgumentException(String.Format("Argument amount is less than zero: {0}", amount));
        _Score += amount;
    }
    public void DecreaseScore(int amount)
    {
        if (amount < 0)
            throw new ArgumentException(String.Format("Argument amount is less than zero: {0}", amount));
        _Score -= amount;
    }

    public void StartGame()
    {
        if (_GameFlow != GameFlow.Ready)
            throw new UnityException(String.Format("Wrong _GameFlow value: {0}", _GameFlow));
        _GameFlow = GameFlow.Playing;

        Instantiate(PlayerPrefab, GameObject.Find("Canvas").transform.position, Quaternion.identity);

        for (int i = 0; i < 2; i++)
        {
            GameObject molecule1 = CreateMolecule(4, new Vector3(100, 100, 0));
            molecule1.GetComponent<Molecule>().Velocity = new Vector2(
                UnityEngine.Random.Range(-1f, 1f),
                UnityEngine.Random.Range(-1f, 1f)) * 40;
        }
    }
    public void FinishGame()
    {
        if (_GameFlow != GameFlow.Playing)
            throw new UnityException(String.Format("Wrong _GameFlow value: {0}", _GameFlow));
        _GameFlow = GameFlow.Finished;

        _TotalScoreUI.GetComponent<Text>().text = String.Format("Total Score: {0}", _Score);
    }
    public void ResetGame()
    {
        // 여기서 Garbage를 열심히 버린다.
        foreach (GameObject v in _MoleculeAlivelist)
            Destroy(v);
        foreach (GameObject v in _MoleculeGarbage)
            Destroy(v);
        foreach (GameObject v in _PhotonAlivelist)
            Destroy(v);
        foreach (GameObject v in _PhotonGarbage)
            Destroy(v);

        _MoleculeAlivelist.Clear();
        _MoleculeGarbage.Clear();
        _PhotonAlivelist.Clear();
        _PhotonGarbage.Clear();
        _Score = 0;

        if (_GameFlow != GameFlow.Finished)
            throw new UnityException(String.Format("Wrong _GameFlow value: {0}", _GameFlow));
        _GameFlow = GameFlow.Ready;
    }

    public void CollisionOccured(GameObject lhs, GameObject rhs)
    {
        CollisionSE.Play();
        if (lhs.tag == "Player")
        {
            if (rhs.GetComponent<Molecule>().size < 1)
            {
                _Alert = true;
                return;
            }

            int rhsSize = rhs.GetComponent<Molecule>().size;
            float playerMag = lhs.GetComponent<Rigidbody2D>().velocity.magnitude;
            Vector3 rhsPosition = rhs.transform.position;
            Vector2 rhsVelocity = rhs.GetComponent<Molecule>().Velocity;
            GameObject c1 = CreateMolecule(rhsSize - 1, rhsPosition);
            GameObject c2 = CreateMolecule(rhsSize - 1, rhsPosition);
            DestroyMolecule(rhs);

            for (int i = 0; i < rhsSize; i++)
                CreatePhoton(rhsPosition);

            float rot = 0;
            float vel = 0;

            rot = -UnityEngine.Random.Range(1f/6, 1f/3) * Mathf.PI;
            c1.GetComponent<Molecule>().Velocity = -new Vector2(
                rhsVelocity.x * Mathf.Cos(rot) + rhsVelocity.y * Mathf.Sin(rot),
                -rhsVelocity.x * Mathf.Sin(rot) + rhsVelocity.y * Mathf.Cos(rot));
            vel = c1.GetComponent<Molecule>().Velocity.magnitude;
            c1.GetComponent<Molecule>().Velocity *= (vel + playerMag) / vel;

            rot *= -1;
            c2.GetComponent<Molecule>().Velocity = -new Vector2(
                rhsVelocity.x * Mathf.Cos(rot) + rhsVelocity.y * Mathf.Sin(rot),
                -rhsVelocity.x * Mathf.Sin(rot) + rhsVelocity.y * Mathf.Cos(rot));
            vel = c2.GetComponent<Molecule>().Velocity.magnitude;
            c2.GetComponent<Molecule>().Velocity *= (vel + playerMag) / vel;

            _Score++;
        }
        else if (lhs.gameObject.tag == "Photon")
        {
            PhotonSE.Play();

            rhs.transform.GetComponent<Player>().speed *= 1.05f;
            DestroyPhoton(lhs.gameObject);
        }
        /*else if (lhs.tag == "Molecule")
        {
            DestroyMolecule(lhs);
            DestroyMolecule(rhs);
            CreateMolecule(lhs.GetComponent<Molecule>().size + rhs.GetComponent<Molecule>().size, new Vector3(0, 0, 0));
        }*/
        else
            throw new ArgumentException(String.Format("Wrong lhs.tag: {0}", lhs.tag));
    }
}
