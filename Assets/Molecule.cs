﻿using UnityEngine;
using System.Collections;

public class Molecule : MonoBehaviour {

	public int size;								// 입자 사이즈

	private Vector2 _velocity;				// 현재 속도. 속도를 변경할 때마다 갱신해 주세요
	private int _id;

	public int ID {
		get { return _id; }
		set { _id = value; }
	}

	public Vector2 Velocity {
		get { return _velocity; }
		set {
            GetComponent<Rigidbody2D>().velocity = value;
            _velocity = value;
        }
	}

	private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
		rb = transform.GetComponent<Rigidbody2D>();
		//_velocity = rb.velocity;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Reset(int id, Sprite sprite, float radius, int size) {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        this.gameObject.GetComponent<CircleCollider2D>().radius = radius;
        this._id = id;
        this.size = size;
        this.gameObject.SetActive(true);
    }

	void OnCollisionEnter2D(Collision2D c) {
		if (c.transform.tag.Equals("xWall")) {
			if (size == 0) {
				int rand = Random.Range(0, 100);

				// 확률적으로 벽을 뚫는다
				if (rand < 20) {
				}
			}

	        rb.velocity = new Vector2(-_velocity.x, _velocity.y);
            Velocity = rb.velocity;
			GameObject.Find("FlowManager").GetComponent<FlowManagerScript>().MolculeHitsTheWall();
		}
		else if (c.transform.tag.Equals("yWall")) {
			if (size == 0) {
				int rand = Random.Range(0, 100);

				// 확률적으로 벽을 뚫는다
				if (rand < 20) {
				}
			}

			rb.velocity = new Vector2(_velocity.x, -_velocity.y);
            Velocity = rb.velocity;
			GameObject.Find("FlowManager").GetComponent<FlowManagerScript>().MolculeHitsTheWall();
		}
		else if (c.transform.tag.Equals("Molecule")) {
			if (_id > c.transform.GetComponent<Molecule>().ID) {
				//GameObject.Find("GameManager").GetComponent<GameManagerScript>().CollisionOccured(this.gameObject, c.gameObject);
			}
		}
		else if (c.transform.tag.Equals("Border")) {
			Destroy(transform.gameObject);
		}
	}
}
