﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public delegate void OnMeltdownEvent();

public class FlowManagerScript : MonoBehaviour {

	public GameObject talmoBeam;

	private OnMeltdownEvent _eventMeltdown;
	private int _meltdownLevel;
	private int _collisionCount;

	private float _flowTime;
	private float _privateTime;

    private GameObject _PressUI;
    private GameObject _talmo;

    private float _targetTimeScale;

	private readonly float criticalPressure = 15;

	// Use this for initialization
	void Start () {
        _targetTimeScale = Time.timeScale;
        _PressUI = GameObject.Find("Press");
	}
	
	// Update is called once per frame
	void Update () {
		if (_flowTime >= 0.5f) {
			float pressure = _collisionCount / _flowTime;
			_collisionCount = 0;
			_flowTime = 0;
            _PressUI.GetComponent<Text>().text = pressure.ToString();
            if (pressure >= criticalPressure) {
				// 액션 시작
				if(_eventMeltdown == null)
					_eventMeltdown = MeltDown;

				// 게임 단계 증가
				_meltdownLevel++;
                GameObject[] molecules = GameObject.FindGameObjectsWithTag("Molecule");
                foreach (GameObject molecule in molecules)
                    if (molecule.GetComponent<Molecule>().size == 0)
                    if (UnityEngine.Random.value < 0.7f)
                        GameObject.Find("GameManager").GetComponent<GameManagerScript>().DestroyMolecule(molecule);
            }
		}
		else {
			_flowTime += Time.deltaTime;
		}

		if(_eventMeltdown != null)
			_eventMeltdown();

        if (Time.timeScale > _targetTimeScale)
            Time.timeScale -= 0.01f;
        else if (Time.timeScale < _targetTimeScale)
            Time.timeScale += 0.01f;
	}

	public void MolculeHitsTheWall() {
		_collisionCount++;
	}

	void MeltDown() {
		_privateTime += Time.deltaTime;
		//Debug.Log(_privateTime);
		if (_talmo == null) {
			_talmo = Instantiate(talmoBeam);
			_talmo.transform.SetParent(GameObject.Find("Canvas").transform);
			RectTransform rect = _talmo.transform.GetComponent<RectTransform>();
			_talmo.transform.localPosition = new Vector3(0, 0, 0);
			_talmo.transform.localScale = new Vector3(1.0f,  1.0f, 1);
            _targetTimeScale = 0.5f;
		}
		else {
			_talmo.transform.localScale = new Vector3(_privateTime / 16f + 1.0f, _privateTime / 16f + 1.0f, 1);
			Image talmoImage = _talmo.transform.GetComponent<Image>();
			Color c = talmoImage.color;
			c.a *= 1f - _privateTime/16f;
			talmoImage.color = c;
		}

		if (_privateTime > 1.5f) {
			Destroy(_talmo.gameObject);
			_talmo = null;
			_eventMeltdown = null;
			_targetTimeScale = 1.0f;
			_privateTime = 0;
		}
	}
}
