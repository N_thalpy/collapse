﻿using UnityEngine;
using System.Collections;

public class Photon : MonoBehaviour {

	private Vector2 _velocity;
	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = transform.GetComponent<Rigidbody2D>();

		Vector2 initVelocity = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
		initVelocity.Normalize();
		initVelocity *= Random.Range(50, 150);

		_velocity = initVelocity;
		rb.velocity = initVelocity;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Reset() {
        if (rb == null)
            rb = transform.GetComponent<Rigidbody2D>();

        Vector2 initVelocity = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        initVelocity.Normalize();
        initVelocity *= Random.Range(50, 150);

        _velocity = initVelocity;
        rb.velocity = initVelocity;
    }

	void OnCollisionEnter2D(Collision2D c) {
		if (c.transform.tag.Equals("xWall")) {
			rb.velocity = new Vector2(-_velocity.x, _velocity.y);
			_velocity = rb.velocity;
		}
		else if (c.transform.tag.Equals("yWall")) {
			rb.velocity = new Vector2(_velocity.x, -_velocity.y);
			_velocity = rb.velocity;
		}
		else if (c.transform.tag.Equals("Player")) {
            GameObject.Find("GameManager").GetComponent<GameManagerScript>().CollisionOccured(this.gameObject, c.gameObject);
			c.transform.GetComponent<Player>().speed *= 1.1f;
		}
	}
}
