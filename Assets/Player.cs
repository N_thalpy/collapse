﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public int size;
	public float speed;
	public float maxSpeed;

	private bool _mouseClick;
	private Vector2 _velocity;
	private Vector3 _oldMousePosition;
	private Vector3 _curMousePosition;
	private Vector3 _forceVector;
	private Rigidbody2D rb;

	private bool keyW, keyS, keyA, keyD;

	public Vector2 Velocity {
		get { return _velocity; }
		set { _velocity = value; }
	}

	public Vector2 ForceVector {
		get { return new Vector2(_forceVector.x, _forceVector.y); }
	}

	// Use this for initialization
	void Start () {
		rb = transform.GetComponent<Rigidbody2D>();
		_velocity = rb.velocity;
	}
	
	// Update is called once per frame
	void Update () {
        /*
		if (Input.GetMouseButtonDown(0)) {
			_oldMousePosition = Input.mousePosition;
			_mouseClick = true;
		}
		if (_mouseClick) {
			_curMousePosition = Input.mousePosition;

			if (_curMousePosition.sqrMagnitude < 10) {
				_forceVector = _curMousePosition - _oldMousePosition;
			}
			else {
				_forceVector = _curMousePosition - _oldMousePosition;
				_forceVector.Normalize();
				_forceVector *= 10;
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			_mouseClick = false;
			rb.AddForce(new Vector2(_forceVector.x, _forceVector.y), ForceMode2D.Impulse);
		}
		*/

        if (speed < 50) {
            GameObject.Find("GameManager").GetComponent<GameManagerScript>().FinishGame();
            DestroyObject(this.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.W)) {
			_velocity += Vector2.up;
		}
		if (Input.GetKeyDown(KeyCode.S)) {
			_velocity -= Vector2.up;
		}
		if (Input.GetKeyDown(KeyCode.A)) {
			_velocity -= Vector2.right;
		}
		if (Input.GetKeyDown(KeyCode.D)) {
			_velocity += Vector2.right;
		}
		if (Input.GetKeyUp(KeyCode.W)) {
			_velocity -= Vector2.up;
		}
		if (Input.GetKeyUp(KeyCode.S)) {
			_velocity += Vector2.up;
		}
		if (Input.GetKeyUp(KeyCode.A)) {
			_velocity += Vector2.right;
		}
		if (Input.GetKeyUp(KeyCode.D)) {
			_velocity -= Vector2.right;
		}

		if (speed > maxSpeed) {
			speed = maxSpeed;
		}

		rb.velocity = Vector3.Normalize(_velocity) * speed;
		
	}

	void OnCollisionEnter2D(Collision2D c) {
		/*
		if (c.transform.tag.Equals("xWall")) {
			rb.velocity = new Vector2(-_velocity.x, _velocity.y);
			_velocity = rb.velocity;
		}
		else if (c.transform.tag.Equals("yWall")) {
			rb.velocity = new Vector2(_velocity.x, -_velocity.y);
			_velocity = rb.velocity;
		}
		*/
		if (c.transform.tag.Equals("Molecule")) {
			GameObject.Find("GameManager").GetComponent<GameManagerScript>().CollisionOccured(this.gameObject, c.gameObject);
			speed *= 0.9f;
		}
	}
}
